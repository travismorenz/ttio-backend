const Game = require("./game");
const storage = require("node-persist");

class Node {
  constructor(parent, movestring = "") {
    this.parent = parent ? parent.movestring : "null";
    this.movestring = movestring;
    this.n_wins = 0;
    this.n_sims = 0;
    this.terminal = false;
    this.children = null;
  }
}

class Tree {
  /** Init tree */
  constructor(initialIterations, searchIterations, allnodes) {
    this.game = new Game();
    this.nodeCount = 0;
    if (allnodes) {
      this.allnodes = allnodes;
      this.root = allnodes[""];
    } else {
      this.root = new Node(null, "");
      this.nodeCount++;
      this.allnodes = {
        "": this.root,
        null: null
      };
    }

    // # of iterations initially and for each following search
    this.initialIterations = initialIterations;
    this.searchIterations = searchIterations;

    // run initial search if tree is new
    if (!allnodes) {
      this.search(this.root, this.initialIterations);
    }
  }

  /** Run MCTS */
  search(root, iterations) {
    while (iterations > 0) {
      const selectedNode = this.select(root);
      const selectedChild = this.expand(selectedNode);
      const result = this.simulate(selectedChild);
      this.propogate(selectedChild, result);
      iterations--;
    }
  }

  /** Select an expandable node*/
  select(node) {
    if (typeof node === "string") {
      node = allnodes[node];
    }
    while (true) {
      const { children, terminal } = node;

      // Return node if node has no children or is terminal
      if (children === null || terminal) {
        return node;
      }

      // Otherwise traverse down tree using UpperConfidenceBound selection
      let highestUCB = [null, -1];

      for (let i = 0; i < children.length; i++) {
        const child = this.allnodes[children[i]];
        // Prioritize children with no simulations
        if (child === null) {
          const newChild = new Node(node, children[i]);
          this.nodeCount++;
          this.allnodes[children[i]] = newChild;
          return newChild;
        }
        const UCB = calcUCB(child, this);
        if (UCB > highestUCB[1]) highestUCB = [child, UCB];
      }

      // Mirrors a recursive call
      node = highestUCB[0];
    }

    function calcUCB(n, that) {
      const w = n.n_wins;
      const s = n.n_sims;
      const p = Math.log(that.allnodes[n.parent].n_sims);
      const c = Math.sqrt(2);
      return w / s + c * Math.sqrt(p / s);
    }
  }

  /** Expand node and return the first created child */
  expand(node) {
    const { movestring, terminal } = node;

    if (terminal) return node;

    // List of legal moves that will be used to build children nodes
    const legalMoves = this.game.load(movestring).legalMoves();

    // Return node if movestring was terminal
    if (typeof legalMoves === "string") {
      node.terminal = legalMoves;
      return node;
    }

    // Array to hold children references
    node.children = [];

    // Populate children
    for (let i = 0; i < legalMoves.length; i++) {
      const move = legalMoves[i];

      // Add child node to allnodes object
      this.allnodes[movestring + move] = null;

      // Add child node key in children array
      node.children.push(movestring + move);
    }

    const randomMS =
      node.children[Math.floor(Math.random() * node.children.length)];

    const chosenNode = new Node(node, randomMS);

    this.allnodes[randomMS] = chosenNode;

    return chosenNode;
  }

  /** Simulate game from a node and return result*/
  simulate(node) {
    const { movestring, terminal } = node;

    if (terminal) return terminal;

    return this.game.load(movestring).simulate();
  }

  /** Propogate results back up tree */
  propogate(node, result) {
    while (node !== null) {
      const { movestring } = node;

      const player = movestring.slice(-1);
      if (result !== "d") {
        if (player === result) {
          node.n_wins++;
        }
      } else {
        node.n_wins += 0.5;
      }

      node.n_sims++;

      node = this.allnodes[node.parent];
    }
  }

  /** Traverse down tree until movestring node is reached */
  traverse(movestring) {
    return this.allnodes[movestring];
  }

  getLegalMoves(movestring) {
    return this.game.load(movestring).legalMoves();
  }

  /** Returns child node with highest number of simulations i.e. the best move */
  getBestMove(node) {
    const { children } = node;
    let bestChild = [null, 0];
    for (let i = 0; i < children.length; i++) {
      const child = this.allnodes[children[i]];
      if (child.n_sims > bestChild[1]) bestChild = [child, child.n_sims];
    }
    return bestChild[0].movestring;
  }

  /** Traverse to input move, run MCTS, return best response move */
  getResponseMove(movestring) {
    const moveNode = this.traverse(movestring);
    this.search(moveNode, this.searchIterations);
    if (moveNode.terminal) {
      return moveNode;
    }
    return this.getBestMove(moveNode);
  }
}

// node --max-old-space-size=3000 server.js

class SaveLoad {
  init() {
    return storage.init();
  }

  load() {
    return storage.getItem("nodes");
  }

  save(nodes) {
    storage.setItem("nodes", nodes).then(() => {
      console.log("DONE SAVING...");
    });
  }
}

module.exports = { SaveLoad, Tree };
