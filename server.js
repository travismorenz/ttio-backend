const app = require("express")();
const { Tree, SaveLoad } = require("./mcts.js");
const cors = require("cors");

app.use(cors());

const sl = new SaveLoad();
sl.init().then(() => {
  sl.load().then(nodes => {
    const tree = new Tree(0, 10000, nodes);
    console.log("Alright, bring it on!");
    app.get("/", (req, res) => {
      const { movestring } = req.query;
      console.log("RECEIVED: " + movestring);
      const response = tree.getResponseMove(movestring);
      const followups = tree.getLegalMoves(response);
      console.log("SENDING: " + response);
      res.json({ movestring: response, followups });
    });
    /*setInterval(() => {
      console.log("SAVING...");
      sl.save(tree.allnodes);
    }, 180000);
    */
  });
});

const PORT = process.env.PORT | 4000;
app.listen(PORT, () => console.log("Starting server..."));
