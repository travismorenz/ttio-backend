/** Class representing the game board. */
class Game {
  /** Generate and return the initial game state. */
  constructor() {
    this.board = [];
    this.lastMove = null;

    this.freshBoard();
  }

  /** Sets up new board */
  freshBoard() {
    this.board = [];
    for (let i = 0; i < 9; i++) {
      const minigame = [];
      for (let k = 0; k < 10; k++) {
        minigame.push(" ");
      }
      this.board.push(minigame);
    }
  }

  /** Set gamestate to movestring */
  load(movestring) {
    this.freshBoard();
    if (movestring !== "") {
      movestring = movestring.match(/.{1,3}/g);
      for (let i = 0; i < movestring.length; i++) {
        this.makeMove(movestring[i]);
      }
    }
    return this;
  }

  /** Return the current player’s legal moves from given state. */
  legalMoves() {
    const { board, lastMove } = this;
    const moves = [];
    const terminal = this.getWinner();

    if (terminal) {
      return terminal;
    }

    // Initially all moves are possible for x
    if (!lastMove) {
      for (let i = 0; i < 9; i++) {
        for (let k = 0; k < 9; k++) {
          let move = "";
          move += i;
          move += k;
          move += "x";
          moves.push(move);
        }
      }
      return moves;
    }

    // Get correct turn
    const turn = lastMove.charAt(2) === "x" ? "o" : "x";

    // Evaluate minigame that next move will be played in and update
    const minigameN = parseInt(lastMove.charAt(1));
    let minigame = board[minigameN];

    // If minigame is not over, find all available plays
    if (minigame[9] === " ") {
      addMoves(minigameN, minigame);
      return moves;
    }

    // If minigame is over
    // Iterate through remaining non-over minigames, adding available plays
    for (let i = 0; i < 9; i++) {
      if (i !== minigameN) {
        minigame = board[i];
        if (minigame[9] === " ") {
          addMoves(i, minigame);
        }
      }
    }
    return moves;

    function addMoves(n, g) {
      for (let i = 0; i < 9; i++) {
        if (g[i] === " ") {
          moves.push("" + n + i + turn);
        }
      }
    }
  }

  /** Advance the given state and return it. */
  makeMove(move) {
    const minigame = parseInt(move.charAt(0));
    const pos = parseInt(move.charAt(1));
    const val = move.charAt(2);

    this.board[minigame][pos] = val;
    this.board[minigame] = this.evaluate(this.board[minigame]);

    this.lastMove = move;
    return this.board;
  }

  /** Make a random legal move */
  makeRandomMove() {
    const moves = this.legalMoves();
    const move = moves[Math.floor(Math.random() * moves.length)];
    this.makeMove(move);
  }

  /** Get winner of the game. */
  getWinner() {
    const condensedBoard = [];

    for (let i = 0; i < 9; i++) {
      condensedBoard.push(this.board[i][9]);
    }
    condensedBoard.push(" ");

    const winner = this.evaluate(condensedBoard)[9];
    return winner !== " " ? winner : false;
  }

  /** Check input game for win/draw and return updated game array */
  evaluate(game) {
    if (game[9] !== " ") {
      return game;
    }
    const winningStates = [];
    const [zero, one, two, three, four, five, six, seven, eight] = game;

    winningStates.push(zero + one + two);
    winningStates.push(three + four + five);
    winningStates.push(six + seven + eight);
    winningStates.push(zero + three + six);
    winningStates.push(one + four + seven);
    winningStates.push(two + five + eight);
    winningStates.push(zero + four + eight);
    winningStates.push(two + four + six);

    for (let i = 0; i < 8; i++) {
      if (winningStates[i] === "xxx") {
        game[9] = "x";
        return game;
      }
      if (winningStates[i] === "ooo") {
        game[9] = "o";
        return game;
      }
    }

    if (!game.slice(1, 9).includes(" ")) {
      game[9] = "d";
      return game;
    }

    return game;
  }

  /** Simulate game using random moves until winner is reached */
  simulate() {
    while (!this.getWinner()) {
      this.makeRandomMove();
    }
    return this.getWinner();
  }

  /** Print board to console */
  printBoard() {
    const [g0, g1, g2, g3, g4, g5, g6, g7, g8, g9] = this.board;

    let board = "";
    board += printRow(g0, g1, g2);
    board += "---------------\n";
    board += printRow(g3, g4, g5);
    board += "---------------\n";
    board += printRow(g6, g7, g8);

    console.log(board);

    function printRow(g0, g1, g2) {
      let row = "";
      row +=
        g0[0] +
        g0[1] +
        g0[2] +
        " | " +
        g1[0] +
        g1[1] +
        g1[2] +
        " | " +
        g2[0] +
        g2[1] +
        g2[2] +
        "\n" +
        g0[3] +
        g0[4] +
        g0[5] +
        " | " +
        g1[3] +
        g1[4] +
        g1[5] +
        " | " +
        g2[3] +
        g2[4] +
        g2[5] +
        "\n" +
        g0[6] +
        g0[7] +
        g0[8] +
        " | " +
        g1[6] +
        g1[7] +
        g1[8] +
        " | " +
        g2[6] +
        g2[7] +
        g2[8] +
        "\n";
      return row;
    }
  }
}

module.exports = Game;
